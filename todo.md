# TODO

The items are either questions, considerations, or things (to do).

## Recent kerning

- Review Aj qA
- /F/f/F review, haha
- consider /ldot /C and so on

## Check again things changed lately

## Questions to resolve


## Considerations

- consider adding SWUNG DASH and using that for
  /tildedotabove /tildedotbelow
- consider /hyphen for lowercase
- consider guillemet for U.C.


## THINGS TO DO

- /summation should have a thin stroke top?
- re-read https://glyphsapp.com/learn/localize-your-font-catalan-punt-volat
- review Catalan /Ldot /ldot
- review weight of /z
- review spacing marks (/tilde U+02DC and so on) for consistent advance width
- /one check spacing
- expand 5-point /asterisk morph to /asterism /lowasterisk and so on
- review and revise /hyphen /endash /emdash: /hyphen is U.C., other dashes are l.c.
  including /K + dashes spacing.

- find testers for catalan /ldot; sami /ezh;
- **r**: condense / contract
- **ð**: contrast with **d**


## Todo after next release

- consider OTL feature for high mark placement on /h /k /b
- Consider uncial form for **e** **U**
- consider /cent.osf

- consider round/chamfer the serifs
- consider remodel the serif entirely
- consider use Glyphs corner components

- redraw ogonek for /oogonek[Simples]

## Done

- r_t resolve spacing [reviewed and left as-is]
- /x and /X sidebearings [rechecked 2024-06]
- /exclam sidebearing [rechecked 2024-06]
- /comma /semicolon sidebearing [rechecked 2024-06]

# END
