# Proofing notes


Glyph Palette subsections:

- letters
- letters.ss10
- numbers, punctuation, symbols

## Letters

Currently the letters subsection is:

from the Latin script:

- Basic
- Western, Central, South Eastern European
- South American
- Oceanian
- Sámi
- Esperanto

restricted to Letter.

In Glyphs you can Shift/Command click in the Languages section,
then Command click on CATEGORIES > Letter.




## commands

`glox` should be used to make the glyph palette

    ff=ArhinniLAB20221130-Mane.otf
    for a in [123]*.list
    do
      glox -xlimit 12800 -glyph "$(sed 's/$/,/' $a)" "$ff" > ${a%.list}.svg
    done

convert to PDF using

    for a in [123]*.svg; do rsvg-convert -f pdf -o ${a%.svg}.pdf $a; done

with dimensions:

    rsvg-convert --format pdf --height 180mm --left 30mm --top 15mm --page-width 297mm --page-height 210mm 10letters00.svg |
    tee t.pdf | pdfinfo -

# END
