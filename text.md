A font running free with the wild herd.
Legs stretched as it enjoys the freedom of the open space.
the downs, the heath, the plain.

When you ride shoulder to shoulder with your friends in the ride.
Our hooves beat together, the rhythm of the herd drums the ground.
Our sound is thunder, rolling out across the hills.
Together strong, loyal to one another, together unstoppable.
Hear us coming, we are the next wave.
