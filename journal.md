# Journal

## 2022-10-09

**n**: at some point i changed it to use flipped serifs on the
top.

**o**: new based on a calligraphic stroke at moderately steep
angle and a diamond/lozenge outline (later widened)

## 2022-10-10

Little (lower case)

- **a**: is a single storey construction, adding a vertical
  stroke to the **o**. No clearing on foot required.

- **o**: widened. And the **a** was also widened in the same
  manner.

**h** **i** **b** **l** **d** **u** are fairly predictable constructions
from **o** **n**.

- **b**: foot is notched to clear bowl away from vertical stem.

- **d**: at the top junction of the vertical stem with the bowl
  stroke, a significant amount of clearing.
  Compared to **b**, foot is not notched, angular slope on bowl
  (inherited from **o**) leaves plenty of space. Same as **a**.

- **r**: is a predictable construction: a sawn-off **n** with a
_tick_ terminal added.

- **e** and **s** are somewhat draft, both based on remodelled
  **o**

- **u** is **n** rotated and the serifs made to point left, with
  a cheeky scallop


Capital (upper case)

- **H** initial design: fairly narrow;
  slight increase in stroke thickness (compared to **n**/**l**);
  decorative crossbar based on brass horse-hardware
  (https://duckduckgo.com/?t=ffab&q=horse+breastplate&iax=images&ia=images)

- **O**: The outline is a scaled **O** with a counter larger
  than the scale would suggest in order to diminish the stroke.
  The stroke width being slightly thicker than the little
  letters.

Cyrillic

- Added **Multiocular o** (and **Monocular o** as a component).
  My latest fun discovery in the Unicodes.


Analphabetics

- **’** add a placeholder `quoteright`.


## 2022-10-11

- **&** `ampersand` unfinished but left in for later rework

- **ð** `eth` drawn as a trial version to check there is enough
  design distance between **eth** and **d**

- **E** drawn

## 2022-10-12

- **r** terminal flipped horizontal to make it flat on the
  right; remodelled slightly.
  Notch cleared slightly by lowering the vertex.

- **&** drawn again from pen-and-paper
- **c** trial drawing to test-drive the terminal (shared with r).
- **s** added terminals based on r and c

## 2022-10-12

Submitted

## 2022-10-13

- **o** Canted **o** to the left, moving its top point left and
  its right point right. This is **o.aa03**
- **a** a.aa03 developed from **o**
- **b** developed from **o**, small clear on bottom notch
- **d** developed from **o**, small clear on top junction; no
  clear on bottom notch
- **ð** crudely redeveloped from **o**
- **R** simplified junction, keeping main shape; open counter
- **O** cant to left following o

## 2022-10-15

- **c** removed bottom terminal (Teach feedback); remodel
- **e** remodel from **c**, close counter (Teach feedback)
- **s** remove top terminal (Teach feedback)
- **r** remove terminal and remodel from **o** (Teach feedback)
- **n** remodel after angular **o**
- **h** **u** remodel after **n**

## 2022-10-17

- **n** and so on: fix a bug where the serifs were not the same!
- **i** split into idotless components, enlarge dot, remodel top
  serif (Teach feedback)
- **g** first order draft

## 2022-10-18

- **v** First order draft (seems narrow, made it wider)

## 2022-10-19

- **K** first order draft
- **g** moved tail to left
- **b** fixed drawing bug
- **t** first order draft
- **f** first order draft
- **w** first order draft
- **k** first order draft

Spacing, using “Designing Fonts”

- **o** changed from 50 o 50 to 45 o 45
- **n** changed from 38 n 38 to 53 n 53 then to 58 n 48
- **h** **a** **b** **d** updated
- **c** changed from 50 c 20 to 50 c 40
- **ð** changed to =o ð 60
- **e** changed to =c e 45
- **g** changed to =o g 70
- **l** changed to =(h-20) l =n (!)
- **r** changed to =n r 1 (!)
- **s** changed to 45 s 40
- **u** changed to =|n u =d
- **v** reviewed with no change. unhappy.

Spacing caps against little letters

- **H** changed to 45 H 45
- **O** changed to 38 O 38
- **R** changed to =H R -20 (kept RSB)
- **K** changed to =H K -20 (kept RSB)
- **E** changed to 35 E 25

## 2022-10-20

- **a** added two-storey a.ss05 as a draft; reduced its median
  stroke to be same as **e**
- **⁊** add U+204A TIRONIAN SIGN ET

## 2022-10-21

- **.** add period and U+00B7 MIDDLE DOT
- **t** somewhat tweaked; shorter tail, taller top
- **f** tweaked; more grey value on right of bottom serif
- **A** first draft
- /omultiocular-cy redrawn using components

## 2022-10-24

- **R** remove the very short extension at
  the junction of bowl and leg (Teach feedback)
- **H** move existing design to H.ss10 and redraw with plain
  crossbar
- /Hbar added
- use a serif component in /idotless
- repeat component on /l /b /d /h /k
- **l** add left-hand foot (Teach feedback)
- **g** terminate tail in vertical cut non-tapered (old design
  in /g.w43) (Teach feedback)
- **v** replace outside edges of serifs with gentle curve,
  thicken on inside

## 2022-10-25

- **w** tweak to follow new /v
- **6** drew following "MAGYARORSZÁG TÖRTÉNETI EMLÉKEI 896-1896"
- **0** blagged from /six , then radicalised
- **7** zeroth order draft
- **1** zeroth order draft
- **2** zeroth order draft, i quite like this
- **5** zeroth order draft, barely worth saving
- **9** tweaked a rotated /six
- **8** drawn
- **3** drawn, unhappily; redrawn with a flat top

## 2022-10-26

- **E** Changed hoof serifs so the flats face to the right,
  thinned arms to a thick-thin-thick structure.
- **M** first draft of the ss10 version
- **x** zeroth other draft
- **y** drawn following v
- **zero** redrawn somewhat after Ondine (Frutiger)
- **ð** Cleared junction, copying from /d, curve top arc over
  to the left
- **m** Drawn from a slightly compressed /n
- **4** redraw into something barely acceptable

## 2022-10-27

- **5** redraw more following Ondine
- **2** redraw more following Ondine
- **7** redrawn
- **3** redrawn, copying bottom bowl from /five
- **!** zeroth order draft
- **;** zeroth order draft
- **1** made flag heavier (Teach feedback)
- **2** made point less pointy (Teach feedback)
- **5** flex the top stroke

## 2022-10-30

- **k** add serif (from /K) to arm (Teach feedback)
- **g** add notch to top-right
- **a** copy notch from /g onto /a
- **x** copy serifs from /v

## 2022-10-31

- **x** add serifs to all strokes; very heavy
- **t** skew cross bar (following /f) shorten bottom tail,
  evening out the right-hand interface
- **j** first draft; and /jdotless
- **î** add circumflex and so on
- **E** remove hoof serif on arm and remodel into flare
- **p** rotated the /d and tweaked
- **þ** extended ascender on /p
- **q** rotated /b
- **s** tweaked (H feedback)
- **š** added some caron/haček

## 2022-11-01

- **z** drafted
- **ä** dieresis and friends
- added `ss05` version of /aacute and /acircumflex following https://glyphsapp.com/learn/stylistic-sets
- **ñ** and friends
- **L** drew, based on /E
- **8** tighter belt (Teach feedback)
- **6** diminished bottom bowl, close gap slightly (Teach feedback),
  tweaked.
- **4** remodelled joint to follow /v (Teach feedback)
- **é** acute and friends
- **ç** cedilla and friends
- **M** based on `M.ss10`
- **D** based on /E and /O, plus a few accent forms
- **P** extended open bowl of /R downwards

## 2022-11-02

- **g** draw `g.ss05` 2-ocular form
- **f** narrowen (Teach feedback); similar spacing
- **r** increase RSB (Teach feeedback)
- **R** narrowen by contracting leg (Teach feedback)
- **ò** grave and friends (tweak to increase value)
- **A** draw plain version (with thick-thin contrast)
- tweak acute

## 2022-11-04

- **;** fixed by swapping components! put dot over comma (Teach feedback)
- **å** /aring and friends
- **g** redraw ear (Teach feedback); copied from top vertex of
  bowl of /g !
- **x** diminish top-left and bottom-right serifs (Teach feedback)

## 2022-11-07

- **t** extend crossbar to right (Teach feedback)
- **Ð** extend crossbar to right (Teach feedback),
  and shape like /f

## 2022-11-08

- on numbers, unified the angle cut on horizontal strokes of
  /two /three /four /seven to be more like /five (Teach feedback)
- **æ** first draft, sloped the crossbars so that they align.
- **Æ** first draft; used H-type on the A-part.
  Apex of A approximately over left-pointing flag of E-part.
- **j** Add weight to tail terminal (Teach feedback)
- **F** based on E; lower cross bar, narrower
- **I** leg of H
- **M** bevel inner top corners, slight remodel; copy across to
  M.ss10 (Teach feedback)
- **N** first draft. many redrafts.

- Variable Font with ocular axis for /a and /g

## 2022-11-09

- Variable Font axis for corners / simplicity.
- Added /e /h /i /n /o for [CORN=1]

## 2022-11-10

- Added /d[CORN=1]
- Add /E[SHOW=1]

oh i broke glyphs (fixed by making _all_ the masters compatible)

## 2022-11-11

Remodelled the /E{SHOW=1} for a third iteration that
seems to fix the glitches:
diamond is wind -1, enclosed by a "blind" of wind +1.

On the Horsey glyph the blind encloses the diamond so
the wind number from outside to inside goes:
0 (outside); 1 (inside letter, outside blind);
2 (inside blind, outside diamond); 1 (inside diamond).
Effect: diamond cannot be seen.

On the LIT glyph the blind shrinks so that it encloses no diamonds.
So the wind number from outside to inside a diamond goes:
0 (outside); 1 (inside letter, outside diamond); 0 (inside diamond).
Effect: diamond is "reversed out" appearing as a region of
background inside the foreground of the letter.

## 2022-11-15

Creating Girth primary.

- /a{wdth=600}
- /d{wdth=600}
- /e{wdth=600}
- /h{wdth=600}
- /n{wdth=600}
- /o{wdth=600}
- /s{wdth=600}

- tweak /g (Teach feedback): unify link and bottom-right stroke
  of top bowl

## 2022-11-16

- /T
- /h set LSB to =|n (haha). Teach feedback.
- /cedillacomb shave (Teach feedback)

## 2022-11-21

- /B

## 2022-11-22

- /C (and accents)
- /N.ss10
- /Z
- /longs

## 2022-11-23

- /germandbls extremely zeroth-order-draft

## 2022-11-24

- /germandbls minor redraw

## 2022-11-25

Converted Goggley to Virtual Primary (master).
Note to self, the coordinates of the Virtual Primary are
the same as a real primary would be (or are, if you're replacing
one).

Convert Lit/SHOW to Virtual Primary.

- /tildecomb revise (Teach feedback); more curvy
- /U
- /Q An **O** plus the bottom part of /two, modified
- /J A /U with the left-hand vertical stem removed and the
  terminal modified slightly

## 2022-11-26

- /zero.osf
- /one.osf
- /two.osf
- /three.osf (identical up to vert shift)
- /four.osf (by stretch and adjust)

## 2022-11-27

- /five.osf /seven.osf (identical up to vert shift)
- /six.osf /eight.osf identical
- /nine.osf draft and redraft
- /Q.ss10
- /G
- /EtTironian
- guillemets, plain design

## 2022-11-28

- /EtTironian tweaked: straight top stroke,
  downstroke with a diminished curve, serif top-left
- /S and /S.ss10
- /K.ss10
- /V /W /X /Y
- /Thorn
- /plus /divide
- /venus /earth

## 2022-11-29

- /mercury /mars /jupiter /saturn /uranus /neptune
- U+1F71C ALCHEMICAL SYMBOL FOR IRON ORE
- U+1F720 ALCHEMICAL SYMBOL FOR COPPER ORE

## 2022-11-30

- U+1F721 ALCHEMICAL SYMBOL FOR IRON-COPPER ORE
- U+1F729 ALCHEMICAL SYMBOL FOR TIN ORE
- U+1F72A ALCHEMICAL SYMBOL FOR LEAD ORE
- U+1F72B ALCHEMICAL SYMBOL FOR ANTIMONY ORE
- /B.ss10 /F.ss10 /P.ss10 /Y.ss10 /Z.ss10
- /caroncomb.alt for ď ľ Ľ ť

## 2022-12-03

Very Heavy version of /O /H /A /R

## 2022-12-04

- Very Heavy version of /D /E /S
- Tweak Raven /A (fix top apex)

## 2022-12-05

- Raven /I /L /M /N
- Ligature for /A_R /A_V /M_B using a rule in feature `ss07`
- U+1F73E ALCHEMICAL SYMBOL FOR BISMUTH ORE

## 2022-12-06

- Minor tweak to ALCHEMICAL SYMBOL FOR BISMUTH ORE
- /question /questiondown /colon
- /asterisk and friends

## 2022-12-07

- DOT ABOVE accents (and `ss10` forms)
- Tweak `caroncomb.alt` placement, placing its anchor _on_ the
  stem of the base, as per MV (priv comm).
- COMBINING MACRON and friends
- COMBINING BREVE and friends
- COMBINING COMMA ABOVE and COMBINING COMMA BELOW and friends

## 2022-12-08

- /Saltillo and /saltillo
- /hbar (had /Hbar for ages)
- /schwa draft and redraft (a tweaked rotated /e)
- /Schwa
- fix /caroncomb.alt position on Simples and Girth primaries
  (and again for /l, lol)
- fix position of /shortstrokecomb on /Eth and /hbar
- /gmacron a direct composite
- /ogonekcomb and many friends
- /hungarumlautcomb and friends
- fill in some .ss10 accent combos

## 2022-12-09

- /Dcedilla /dcedilla direct composites
- at some point i addded loclMAH forms for Marshallese but i
  can't find the exact change now
- /dagger and DOUBLE DAGGER
- draft /Ezh /ezh and the same with caron
- add /Tbar and /tbar with minor tweaks to anchors
- redraw /J as descender
- fixing more accent placements

## 2022-12-10

- /ldot /Ldot

## 2022-12-11

- /Oslash /oslash
- /bullet
- U+2E33 RAISED DOT and U+2E34 RAISED COMMA
- U+2Exx half- and turned- daggers
- Add U+2010 HYPHEN

## 2022-12-12

- U+2212 MINUS SIGN
- @ and PILCROW SIGN and U+204B REVERSED PILCROW SIGN
- U+00B0 DEGREE SIGN and composite U+2103 DEGREE CELSIUS
- Œ œ
- filling in some accents
- /eng /Eng (large **n** form) and /Eng for Sami: /Eng.loclNSM
- U+2E40 DOUBLE HYPHEN
- /slash /backslash

## 2022-12-13

- enlarge ring for Alchemical Ores
- Add U+1F731 ALCHEMICAL SYMBOL FOR REGULUS OF ANTIMONY-2
  (dotted circle with cross above)
- U+1F718 ALCHEMICAL SYMBOL FOR ROCK SALT
- U+1F70D ALCHEMICAL SYMBOL FOR SULFUR
- SUN and VERDIGRIS
- AQUA VITAE and ss07 rule
- STRATUM SUPER STRATUM
- STRATUM SUPER STRATUM-2

## 2022-12-14

- tweak to /L top anchor for U+1E38 LATIN CAPITAL LETTER L WITH DOT BELOW AND MACRON
- add /dotbelowcomb accent and the letters that use it
- add specialised `bottom_cedilla` anchor for /h and /H and use it
- /Germandbls
- U+019D LATIN CAPITAL LETTER N WITH LEFT HOOK and its lower case
- Add /kgreenlandic / KRA
- Add /Aringacute and its lower case
- Add /napostrophe

## 2022-12-16

- Fixing Raven as a real Primary

## 2022-12-19

- Add # $
- Add /percent (via /fraction and `frac`)
- Add /one.dnom /two.dnom /four.dnom and friends
- Add U+2030 U+2031 /perthousand /pertenthousand

## 2022-12-20

- Add /three.dnom /five.dnom /six.dnom /seven.dnom /eight.dnom /nine.dnom
  and friends

## 2022-12-21

- Add HYPEN-MINUS
- Add HORIZONTAL ELLIPSIS

## 2022-12-22

- Tweak Ezh (Teach feedback)
- Tweak G (replace crossbar with left-serif)
- Tweak ogonek (Teach feedback)
- Raise bar on /Hbar (Teach feedback)
- Decrease grey value on /one.numr /four.dnom, add serif on
  /one.dnom; separating /one.dnom and /one.numr

## 2022-12-26

- Add /emdash


# END
