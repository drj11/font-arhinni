# Arhinni

_Arhinni_ is my project font for the
ILT Introduction to Latin Type Design (Fall 2022).

The name _Arhinni_ is chosen to be
close to the name of _Rhiannon_ from Welsh mythology.

Weight / style names:

- Mane (aka Regular)
- Raven for wght=800


## Design Notes

The numbers are (currently) intended to be like an adopted
family member or a family friend.
A related style, but not necessarily cognate.


## Hyphens and Dashes

The _true_ hyphen is U+2010 HYPHEN and is drawn correctly:
vertically centered on the /o.
This glyph is called /hyphentwo in Glyphs; it is not present in AGLFN.

Dashes /endash /emdash follow the true hyphen; with small sidebearings.

The ASCII hyphen (is U+002D HYPHEN-MINUS in Unicode) is a compromise:
vertically at height of /minus but in width between true hyphen and /minus.

U+2E1A HYPHEN WITH DIAERESIS is drawn using true hyphen.

Currently U+2E1E TILDE WITH DOT ABOVE and U+2E1F TILDE WITH DOT BELOW
have their tilde component at the same height as dashes.


## Stylistic Sets

- ss04 used for 1-storey forms of /a and /g (2-storey forms are
  primary as of 2022-11)
- ss07 used for "Alchemical Symbol Ligatures" (for letter-based
  alchemical symbols, converts short letter sequences
  into their Alchemical Symbol)
- ss10 used for ring-decorated forms of /H /S and so on (a dozen)


## Astronomical Symbols

Good models and sources for these are hard to come by, but
https://en.wikipedia.org/wiki/Astronomical_symbols is a good
starting place.


## Variable Fonts

Axes:

- CORN (corners)
- OCUL (ocularity = eye count)
- SHOW (showtime = amount lit)
- wdth (standard Width)
- wght (standard Weight)

Current the topology is essentially star-shaped with the primary
_Mane_ at the centre.

Mane is at position:
{CORN=2, OCUL=1, SHOW=0, wdth=430, wght=400}

CORN is a rough indication of how "cornery" the glyphs look.
The typical Mane primary is quite cornery, so has CORN=2.
CORN=1 is a more smooth version.
A totally smooth version (not drawn) would be CORN=0.

Simples is along the CORN axis at position:
{CORN=1, OCUL=1, SHOW=0, wdth=430, wght=400}
[or is it? check that `wght` coordinate]
Only a few letters drawn; not exported.

OCUL (ocularity, count of eyes) is only used for /a.ss04 and /g.ss04.
At OCUL=1 they are the simple one-eye forms.
At OCUL=2 they are the 2-storey (or binocular = 2 eye) forms.
Virtual primary, using bracket layer for /a.ss04 and /g.ss04.
Only exported in Variable Font.

SHOW (showbiz/showtime) is currently only used for /E.
At SHOW=0 no lights are lit.
At SHOW=1 all lights are lit. For /E there are 12 lights
arranged in 3 groups of 4 lights, with the lights in each group
lighting up evenly from 0 to 4.
Virtual primary, using bracket layer for /E.
For demonstration purposes only.

`wdth` is a standard Width axis.
The coordinate position (430 for Horsey) is the nominal width
(at half x-height, so ignoring serifs and so on), of the /n.
This may be an abuse of the wdth axis meaning.

Girth is a primary along the `wdth` axis at position:
{CORN=2, OCUL=1, SHOW=0, wdth=600, wght=400}
(only a few letters drawn)

`wght` is a standard Weight axis.
Raven is a very bold primary at a nominal position:
{CORN=2, OCUL=1, SHOW=0, wdth=430, wght=800}
the drawing is a work in progress.



## Metrics

UPM 1000

- 870 Latin lower case accent top line (within the Ascender overshoot)
- 860 Ascender top line (+16 overshoot)
- 800 Capital top line (+16 overshoot)
- 660 lower case numbers top (+16 overshoot)
- 600 x-height / median line
- 0 baseline
- −260 descender line (-16 overshoot); and
  lower case number bottom line


## Glyph Notes

In 2022-10 glyph colours are used to indicate review priorities:

- *green* priority review (requested by instructor)
- *pink* may review (at own discretion)
- *blank* no need to review (draft, placeholder, subject to change)


## Research

Some notes on numbers.

- French 111 : good
- Diskus : good
- Noris : good, but with large ascender to x-height ratio
- Ondine : good, but lining
- Brush : ok
- Cascade : ok

- Research /eight with broken stroke: Diskus, Gando (French 111
  by Bitstream on myfonts.com), Brush Script (ATF),
  Cascade Script (Carter, 1965),
  Noris Script (Zapf, 1976), Ondine

# END
